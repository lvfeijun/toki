import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';

import 'package:toki/pages/homePage.dart';
import 'package:toki/style/style.dart';

void main() {
  /// 自定义报错页面
  if(kReleaseMode){
    ErrorWidget.builder = (FlutterErrorDetails flutterErrorDetails){
      debugPrint(flutterErrorDetails.toString());
      return Material(
        child: Center(
          child: Text("发生了没有处理的错误\n请通知开发者",
            textAlign: TextAlign.center,),
        ),
      );
    };
  }
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Tito',
      theme: ThemeData(
        //亮度
        brightness: Brightness.dark,
        //提示颜色
        hintColor: Colors.white,
        //强调色
        accentColor: Colors.white,
        //主要色板
        primaryColor: ColorPlate.orange,
        primaryColorBrightness: Brightness.dark,
        scaffoldBackgroundColor: ColorPlate.back1,
        dialogBackgroundColor: ColorPlate.back2,
        accentColorBrightness: Brightness.light,
        //主要色板
        // primarySwatch: Colors.blue,
        textTheme: TextTheme(
          bodyText1: StandardTextStyle.normal,
        ),
      ),
      home: HomePage(),
    );
  }
}
